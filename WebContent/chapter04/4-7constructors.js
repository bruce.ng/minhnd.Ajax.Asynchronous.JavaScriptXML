function Monster(text) {
  /*
   * The purpose of the following code is to increment a global variable for each instance of this class.
   * In the event of the global variable being undefined it will be initialized with a value of one.
   */
  try {
    ++_monster;
  } catch (e) {
    _monster = 1;
  }
  /*
   * This code, which is executed whenever a new instance is created, initializes new occurrences of this object.
   * Private and public properties are defined and initialized. In addition, methods are exposed making them public.
   */
  var occurrence = _monster; // Private property
  this.string = text; // Public property
  this.palendrome = _palendrome; // Public method
  this.number = _number; // Public method
  /*
   * The following function is a method which has been made public by the above:
   * this.palendrome = _palendrome;
   * statement.
   */

  function _palendrome() {
    var re = new RegExp('[ ,.!;:\']{1,}', 'g');
    var text = this.string.toLowerCase().replace(re, '');
    return (text == _reverse(text))
  }
  /*
   * The following function is a public read only method that gets the value of the private property occurrence.
   * Through techniques like this it is possible to maintain control over the inner workings of objects.
   */
  function _number() {
    return (occurrence);
  }
  /*
   * The _reverse function is a private method.
   * Methods are private when they are not exposed using the
   * this.[external name] = [internal name]
   * statement as _palendrome and _number were.
   */
  function _reverse(string) {
    var work = '';
    for (var i = string.length; i >= 0; --i) {
      work += string.charAt(i);
    }
    return (work);
  }
}

/** Demo */
var myMonster = new Monster();
myMonster.string = 'Able was I ere I saw Elba!';
alert(myMonster.string);
alert(myMonster.palendrome());
alert(myMonster.number());

// Using the prototype Property to Create an sclass Constructor

Creature.prototype = new Creature;
Creature.prototype.constructor = Creature;
function Creature() {
  /*
   * The purpose of the following code is to increment a global variable for
   * each instance of this class. In the event of the global variable being
   * undefined it will be initialized with a value of zero.
   */
  try {
    ++_creature;
    /*
     * This is a public property which really shouldn’t be accessed externally.
     */
    this._instance = _creature;
  } catch (e) {
    /*
     * Zero is used here due to the fact that this constructor is executed at
     * class definition time.
     */
    _creature = 0;
  }
}
Creature.prototype.string; // Public property
/*
 * The following function is a method which has been made public by the
 * Creature.prototype.palendrome = _Creature_palendrome; statement below.
 */

function _Creature_palendrome() {
  var re = new RegExp('[ ,.!;:\']{1,}', 'g');
  var text = this.string.toLowerCase().replace(re, '');
  return (text == _reverse(text))
  /*
   * The _reverse function is a private method available only within the
   * enclosing method.
   */
  function _reverse(string) {
    var work = '';
    for (var i = string.length; i >= 0; --i)
      work += string.charAt(i);
    return (work);
  }
}
Creature.prototype.palendrome = _Creature_palendrome;
/*
 * The following function is a method which has been made public by the
 * Creature.prototype.number = _Creature_Number; statement below.
 */
function _Creature_Number() {
  return (this._instance);
}
Creature.prototype.number = _Creature_Number;