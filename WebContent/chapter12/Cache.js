//<![CDATA[
/*
	Class:		Cache
	Function:	Cache
	Purpose:	To act as a client-side cache(associative array).
	          Data are stored as name/value pairs.
*/
function Cache() {
  var _cache = new Object();				// Object to store information
  var _namesArray = new Array();		// Array for names

  this.insert = _insert;				  	// Method: cache an object
  this.retrieve = _retrieve;				// Method: retrieve a cached object
  this.purge = _purge;					    // Method: purge an object/objects
  this.names = _names;					    // Method: return names

  /*
      Function:	_insert
      Method:	insert
      Purpose:	Inserts a name/value pair into the cache.
  */
  function _insert(name,value) {
    _cache[name] = value;				    // Cache object
    _namesArray.push(name);				  // Store name
  }

  /*
      Function:	_retrieve
      Method:	retrieve
      Purpose:	Retrieves a value from the cache using a name.
  */
  function _retrieve(name) {
    if(typeof(_cache[name]) == 'undefined')
      return(null);					       // Object not cached
    else
      return(_cache[name]);			       // Return object
  }

  /*
      Function:	_purge
      Method:	purge
      Purpose:	Purges one or more name/value pairs from the cache.
  */
  function _purge() {
    if(arguments.length == 0) {
      _cache = new Object();			   // Create new cache object
      _namesArray = new Array();		   // Create new names array
    } else {
      var singleName;
      _namesArray = new Array();		   // Create new names array

      for (var i = 0;i < arguments.length; i++) {
        _cache[arguments[i]] = null;
      }

      for (singleName in _cache)
        if(_cache[singleName] != null)
          _namesArray.push(singleName);
    }
  }

  /*
     Function:	_names
     Method:	names
     Purpose:	Returns an array consisting of the names from the cache.
  */
  function _names() {
    return(_namesArray);
  }
}

