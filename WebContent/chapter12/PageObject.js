//<!-- <![CDATA[
PageObject.prototype = new PageObject;
PageObject.prototype.constructor = PageObject;

/*
	Class:			PageObject
	Function:		PageObject
	Method:			n/a
	Description:	Constructor for this class.
*/
function PageObject(systemName,pageName) {
  if(arguments.length != 0)
    this._initialize(systemName,pageName);
}

/*
	Class:			PageObject
	Function:		PageObject_initialize
	Method:			_initialize
	Description:	Initialize page.
*/
function PageObject_initialize(systemName,pageName) {
  var xsl = '<?xml version=\'1.0\'?>';

  this.cache = new Cache();
  this.xmlhttp = new XMLHTTPRequest();
  this.xml = new XMLDocument();
  this.xsl = new XMLDocument();
  this.xslt = new XsltProcessor();
  this.soap = new SOAPEnvelope();

  this.system(systemName);
  this.name(pageName);

  xsl += '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'
  xsl += '<xsl:template match="/">';
  xsl += '<xsl:copy-of select="//soap:Body/*/*/*" />';
  xsl += '</xsl:template>';
  xsl += '</xsl:stylesheet>';

  this.xsl.load(xsl);
  this.cache.insert('rinse',this.xsl.serialize());

  xsl = '<?xml version=\'1.0\'?>';
  xsl += '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">';
  xsl += '<xsl:output method="html" indent="yes" media-type="text/html"/>';
  xsl += '<xsl:param name="systemName" />';
  xsl += '<xsl:param name="pageName" />';
  xsl += '<xsl:template match="/">';
  xsl += '<xsl:element name="table">';
  xsl += '<xsl:attribute name="border">0</xsl:attribute>';
  xsl += '<xsl:attribute name="height">60px</xsl:attribute>';
  xsl += '<xsl:attribute name="width">975px</xsl:attribute>';
  xsl += '<xsl:attribute name="cellpadding">0</xsl:attribute>';
  xsl += '<xsl:attribute name="cellspacing">0</xsl:attribute>';
  xsl += '<xsl:element name="tr">';
  xsl += '<xsl:attribute name="class">pageHeader</xsl:attribute>';
  xsl += '<xsl:attribute name="height">40px</xsl:attribute>';
  xsl += '<xsl:element name="td">';
  xsl += '<xsl:attribute name="width">5%</xsl:attribute>';
  xsl += '<xsl:text> </xsl:text>';
  xsl += '</xsl:element>';
  xsl += '<xsl:element name="th">';
  xsl += '<xsl:attribute name="id">systemName</xsl:attribute>';
  xsl += '<xsl:attribute name="class">pageCell</xsl:attribute>';
  xsl += '<xsl:attribute name="width">45%</xsl:attribute>';
  xsl += '<xsl:attribute name="align">left</xsl:attribute>';
  xsl += '<xsl:value-of select="$systemName" />';
  xsl += '</xsl:element>';
  xsl += '<xsl:element name="th">';
  xsl += '<xsl:attribute name="id">pageName</xsl:attribute>';
  xsl += '<xsl:attribute name="class">pageCell</xsl:attribute>';
  xsl += '<xsl:attribute name="width">45%</xsl:attribute>';
  xsl += '<xsl:attribute name="align">right</xsl:attribute>';
  xsl += '<xsl:value-of select="$pageName" />';
  xsl += '</xsl:element>';
  xsl += '<xsl:element name="td">';
  xsl += '<xsl:attribute name="width">5%</xsl:attribute>';
  xsl += '<xsl:text> </xsl:text>';
  xsl += '</xsl:element>';
  xsl += '</xsl:element>';
  xsl += '<xsl:element name="tr">';
  xsl += '<xsl:element name="td">';
  xsl += '<xsl:text> </xsl:text>';
  xsl += '</xsl:element>';
  xsl += '<xsl:element name="td">';
  xsl += '<xsl:text> </xsl:text>';
  xsl += '</xsl:element>';
  xsl += '<xsl:element name="td">';
  xsl += '<xsl:text> </xsl:text>';
  xsl += '</xsl:element>';
  xsl += '<xsl:element name="td">';
  xsl += '<xsl:text> </xsl:text>';
  xsl += '</xsl:element>';
  xsl += '</xsl:element>';
  xsl += '</xsl:element>';
  xsl += '</xsl:template>';
  xsl += '</xsl:stylesheet>';

  this.xsl.load(xsl);
  this.xslt.importStylesheet(this.xsl.serialize());
  this.xslt.load('<null />');
  this.xslt.setParameter('systemName',this.system());
  this.xslt.setParameter('pageName',this.name());

  document.getElementById('header').innerHTML = this.xslt.transform();

  this.xslt.removeParameter('systemName');
  this.xslt.removeParameter('pageName');
}
PageObject.prototype._initialize = PageObject_initialize;

PageObject.prototype._system;
PageObject.prototype._name;

PageObject.prototype.cache;
PageObject.prototype.xmlhttp;
PageObject.prototype.xml;
PageObject.prototype.xsl;
PageObject.prototype.xslt;
PageObject.prototype.soap;
PageObject.prototype.SOAPAction = 'http://tempuri.org/getXML';
PageObject.prototype.uri = 'http://localhost/AJAX/msas.asmx';
PageObject.prototype.interval = 10;         // Property: timeout interval

/*
	Class:			PageObject
	Function:		PageObject_post
	Method:			post
	Description:	Post SOAP request.
*/
function PageObject_post(object,soapObject) {
  object.setEnvelope(soapObject.envelope());
  object.setRequestHeader('SOAPAction',this.SOAPAction);
  object.setRequestHeader('Content-Type','text/xml');

  object.load(this.uri);
}
PageObject.prototype.post = PageObject_post;

/*
	Class:			PageObject
	Function:		PageObject_system
	Method:			_initialize
	Description:	Get/Set system name for page header.
*/
function PageObject_system() {
  if(arguments.length != 0) {
    this._system = arguments[0];
    document.title = this._system;

    try {
	  document.getElementById('systemName').innerText = this._system;
    }
    catch(e) { }
  } else {
    return(this._system);
  }
}
PageObject.prototype.system = PageObject_system;

/*
	Class:			PageObject
	Function:		PageObject_name
	Method:			_initialize
	Description:	Get/Set page name for the current page.
*/
function PageObject_name() {
  if(arguments.length != 0) {
    this._name = arguments[0];

    try {
	  document.getElementById('pageName').innerText = this._name;
    }
    catch(e) { }
  } else {
    return(this._name);
  }
}
PageObject.prototype.name = PageObject_name;
// ]]> -->