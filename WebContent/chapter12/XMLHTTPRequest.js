//<!-- <![CDATA[
XMLHTTPRequest.prototype = new XMLHTTPRequest;
XMLHTTPRequest.prototype.constructor = XMLHTTPRequest;

/*
	Class:			XMLHTTPRequest
	Function:		XMLHTTPRequest
	Method:			    n/a
	Description:	  Constructor for this class.
*/
function XMLHTTPRequest() {
  this._IE = (new RegExp('internet explorer','gi')).test(navigator.appName);
  this._XMLHTTP;						    // XMLHTTP request object
  this._requestHeader = new Cache();

  if (this._IE)
    this._XMLHTTP = new ActiveXObject('Microsoft.XMLHTTP');
  else
    this._XMLHTTP = new XMLHttpRequest();
}

XMLHTTPRequest.prototype.action = 'GET';	     // Property: action (GET, POST or HEAD)
XMLHTTPRequest.prototype.asynchronous = true;
XMLHTTPRequest.prototype.envelope = null;      // Property: package to send

/*
	Class:			XMLHTTPRequest
	Function:		XMLHTTPRequest_readyState
	Method:			readyState
	Description:	Returns the readyState for the XMLHTTP Request object.
*/
function XMLHTTPRequest_readyState() {
  return(this._XMLHTTP.readyState);
}
XMLHTTPRequest.prototype.readyState = XMLHTTPRequest_readyState;

/*
	Class:			XMLHTTPRequest
	Function:		XMLHTTPRequest_getResponseHeader
	Method:			getResponseHeader
	Description:	Returns a single response header from the last XMLHTTP Request action.
*/
function XMLHTTPRequest_getResponseHeader(name) {
  return(this._XMLHTTP.getResponseHeader(name));
}
XMLHTTPRequest.prototype.getResponseHeader = XMLHTTPRequest_getResponseHeader;

/*
	Class:			XMLHTTPRequest
	Function:		XMLHTTPRequest_getAllResponseHeaders
	Method:			getAllResponseHeaders
	Description:	Returns all of the response headers from the last XMLHTTP Request action.
*/
function XMLHTTPRequest_getAllResponseHeaders() {
  return(this._XMLHTTP.getAllResponseHeaders());
}
XMLHTTPRequest.prototype.getAllResponseHeaders = XMLHTTPRequest_getAllResponseHeaders;

/*
	Class:			XMLHTTPRequest
	Function:		XMLHTTPRequest_responseText
	Method:			responseText
	Description:	Returns the text response from the last XMLHTTP Request action.
*/
function XMLHTTPRequest_responseText() {
  return(this._XMLHTTP.responseText);
}
XMLHTTPRequest.prototype.responseText = XMLHTTPRequest_responseText;

/*
	Class:			XMLHTTPRequest
	Function:		XMLHTTPRequest_responseXML
	Method:			responseXML
	Description:	Returns the XML DOM document response from the last XMLHTTP Request action.
*/
function XMLHTTPRequest_responseXML() {
  if(this._IE) {
    var xml = new ActiveXObject('MSXML2.FreeThreadedDOMDocument.3.0');

    xml.async = true;

    xml.loadXML(this._XMLHTTP.responseText);

    return(xml);
  } else
    return(this._XMLHTTP.responseXML);
}
XMLHTTPRequest.prototype.responseXML = XMLHTTPRequest_responseXML;

/*
	Class:			XMLHTTPRequest
	Function:		XMLHTTPRequest_stateChangeHandler
	Method:			n/a
	Description:	Dummy state change handler for asynchronous requests.
*/
function XMLHTTPRequest_stateChangeHandler() { }
XMLHTTPRequest.prototype.stateChangeHandler = XMLHTTPRequest_stateChangeHandler;

/*
	Class:			setRequestHeader
	Function:		XMLHTTPRequest_setRequestHeader
	Method:			setRequestHeader
	Description:	Inserts to the cache of HTTP request headers.
*/
function XMLHTTPRequest_setRequestHeader(name,value) {
  this.removeRequestHeader(name);
  this._requestHeader.insert(name,value);
}
XMLHTTPRequest.prototype.setRequestHeader = XMLHTTPRequest_setRequestHeader;

/*
	Class:			setRequestHeader
	Function:		XMLHTTPRequest_removeRequestHeader
	Method:			n/a
	Description:	Removes from the cache of HTTP request headers.
*/
function XMLHTTPRequest_removeRequestHeader(name) {
  this._requestHeader.purge(name);
}
XMLHTTPRequest.prototype.removeRequestHeader = XMLHTTPRequest_removeRequestHeader;

/*
	Class:			XMLHTTPRequest
	Function:		XMLHTTPRequest_send
	Method:			send
	Description:	Sends XMLHTTP Request.
*/
function XMLHTTPRequest_send() {
  var successful = false;

  if(arguments.length != 0)
    this.envelope = arguments[0];

  switch(this._XMLHTTP.readyState) {
    case(4):
//      if(this._IE)					         // Complete - IE requires new instance
//		this._XMLHTTP = new ActiveXObject('Microsoft.XMLHTTP');
    case(0):
      try {
        if(this._IE)
		  this._XMLHTTP.onreadystatechange = this.stateChangeHandler;
		else
		  this._XMLHTTP.stateChangeHandler = this.XMLHTTPRequest_stateChangeHandler;

        this._XMLHTTP.open(this.action,this.uri,this.asynchronous);

        var names = this._requestHeader.names();

        for(var i=0;i < names.length;i++)
          this._XMLHTTP.setRequestHeader(names[i],this._requestHeader.retrieve(names[i]));

        this._XMLHTTP.send(this.envelope);

        sucessful = true;
      }
      catch(e) { }

      break;
    default:

      break;
  }

  return(successful);
}
XMLHTTPRequest.prototype.send = XMLHTTPRequest_send;
// ]]> -->

