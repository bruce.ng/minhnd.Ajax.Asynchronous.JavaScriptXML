﻿DELIMITER $$

DROP FUNCTION IF EXISTS `ajax`.`f_xmlNode`$$
CREATE FUNCTION `ajax`.`f_xmlNode`(
/*
  Function:   f_xmlNode
  Programmer: Edmond Woychowsky
  Purpose:    To produce the a text representation of an XML node.
*/
  nodeName VARCHAR(255),                /*  XML node name                      */
  nodeValue LONGTEXT,                   /*  XML node value                     */
  escape BOOLEAN                        /*  Apply XML entity escaping          */
) RETURNS longtext
BEGIN
  DECLARE xml LONGTEXT;                 /*  XML text node/value combination    */

  IF nodeValue IS NULL OR LENGTH(nodeValue) = 0 THEN
    SET xml = CONCAT('<',nodeName,' />');
  ELSE
    IF escape THEN
      SET xml = CONCAT('<',nodeName,'>',REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(nodeValue,'&','&amp;'),'>','&gt;'),'<','&lt;'),'''','&apos;'),'"','&quot;'),'</',nodeName,'>');
    ELSE
      SET xml = CONCAT('<',nodeName,'>',nodeValue,'</',nodeName,'>');
    END IF;
  END IF;

  RETURN xml;
END$$

DELIMITER ;