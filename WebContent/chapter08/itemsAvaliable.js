soap.content = '<guild_item_id>' + parm + '</guild_item_id><guild_id/>';

soap.operator = 'getItems';
xml.setEnvelope(soap.envelope());
xml.setRequestHeader('SOAPAction','http://tempuri.org/getItems');
xml.setRequestHeader('Content-Type','text/xml');
xml.load('http://localhost/AJAX4/chapter4.asmx');

window.setTimeout('pageWait()',10);

pageName = name;


function pageWait() {
  if(xml.readyState() == 4) {
    var xhtml = itemsXHTMLStart;
    var input = document.getElementById('buttons').getElementsByTagName('input');

    if(_IE)
      document.getElementById('xmlDI').XMLDocument.loadXML(xml.selectSingleNode('//NewDataSet').serialize());
    else
      document.getElementById('xmlDI').innerHTML = xml.selectSingleNode('//NewDataSet').serialize();

    switch(pageName) {
      case('Items'):
        for(var i=0;i < xml.selectNodes('//Table').length;i++) {
          var reGuild = new RegExp('@guild','i');
          var reItem = new RegExp('@item','i');
          var guild = xml.selectNodes('//guild_id')[i].serialize().replace(new RegExp('<[^<]{0,}>','g'),'');
          var item = xml.selectNodes('//guild_item_id')[i].serialize().replace(new RegExp('<[^<]{0,}>','g'),'');

          xhtml += itemsInnerXHTML.replace(reGuild,guild).replace(reItem,item);
        }

        document.getElementById('formBody').innerHTML = xhtml + itemsXHTMLEnd;

        break;
      case('Detail'):
        document.getElementById('formBody').innerHTML = detailXHTML;

        break;
    }

    window.setTimeout('_bind()',10);
  } else
    window.setTimeout('pageWait()',10);
}
