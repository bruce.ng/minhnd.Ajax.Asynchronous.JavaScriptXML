<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" media-type="text/html" />
  <xsl:template match="/">
    <select id="myselect" name="myselect">
      <xsl:for-each select="/states/state">
        <xsl:element name="option">
          <xsl:attribute name="value">
			<xsl:value-of select="state_abbreviation" />
		  </xsl:attribute>
          <xsl:value-of select="state_name" />
        </xsl:element>
      </xsl:for-each>
    </select>
  </xsl:template>
</xsl:stylesheet>
